import React from 'react';
import s from './HeaderBlock.module.scss';
//console.log('####: s', s);
import ReactLogoPng from '../../logo.svg';
//console.log('####: ReactLogoPng', ReactLogoPng);
import { ReactComponent as ReactLogoSvg } from '../../logo.svg';

const HeaderBlock = () => {
    return (
        <div className={s.cover}>
            <div className={s.wrap}>
                <h1 className={s.header}>Learn Words Online</h1>
                <img src={ReactLogoPng} />
                <ReactLogoSvg />
                <p className={s.descr}>Use cards for learning</p>
            </div>
        </div>
    );
};

export default HeaderBlock;