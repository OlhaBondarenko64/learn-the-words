// eslint-disable-next-line
import React from 'react';
import ReactDOM from 'react-dom';
import HeaderBlock from './components/HeaderBlock';
import './index.css';

// const el = React.createElement(
//   'h1',
//   null,
//   "Hello World, React.js"
// );
//const el = <h1>Hello World!!!</h1>;
//console.log(el);

// const AppList = () => {
//   return(
//     <ul>
//       <li>My first list</li>
//       <li>My second list</li>
//     </ul>
//   );
// }
const AppList = () => {
  const items = ['item1', 'item2', 'item3', 'item4'];
  const firstItem = <li>Item 0</li>;

  const isAuth = false;

  return(
    <ul>
      { isAuth ? firstItem : null }
      {items.map(item => <li>{item}</li>)}
      <li>{items[0]}</li>
      <li>{items[1]}</li>
    </ul>
  );
};

const AppHeader = () => {
  //const margin = 20;
  // const headerStyle = {
  //     //color: red,
  //     //marginTop: 15px,
  //     marginLeft: `${margin}px`,
  // };
  return(
    //<h1 style={{headerStyle}}>My Header</h1>
    //<h1 style={{color: 'red'}}>My Header</h1>
    <h1 className="header">My Header</h1>
  );
} 

const AppInput = () => {
  const placeholder = "Type text here ...";

  return(
    <label htmlFor="search">
      <input id="search" placeholder={placeholder} />
    </label>
  )
}

const el = (
  // <React.Fragment>
  <>
    <HeaderBlock />
    <AppHeader />
    <AppInput />
    <AppList />
    <AppHeader />
    <AppList />
  </>
  // </React.Fragment>
);

ReactDOM.render(
  el, document.getElementById('root')
);


